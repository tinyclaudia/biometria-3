﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Windows.Interop;
using System.Drawing.Imaging;
using Microsoft.VisualBasic;
using System.ComponentModel;

namespace biometry3
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : System.Windows.Window
    {
        private System.Drawing.Color[,] pixels;
        private int[,] pixelsMatrix;
        private Bitmap bmp;

        private int[] codesToDelete = new int[]{ 3, 5, 7, 12, 13, 14,
           15, 20, 21, 22, 23, 28, 29, 30, 31, 48, 52, 53, 54, 55, 56, 60, 61, 62, 63, 65, 67, 69, 71, 77, 79, 80,
           81, 83, 84, 85, 86, 87, 88, 89, 91, 92, 93, 94, 95, 97, 99, 101, 103, 109, 111, 112,
           113, 115, 116, 117, 118, 119, 120, 121, 123, 124, 125, 126, 127, 131, 133, 135, 141,
           143, 149, 151, 157, 159, 181, 183, 189, 191, 192, 193, 195, 197, 199, 205, 207,
           208, 209, 211, 212, 213, 214, 215, 216, 217, 219, 220, 221, 222, 223, 224, 225, 227,
           229, 231, 237, 239, 240, 241, 243, 244, 245, 246, 247, 248, 249, 251, 252, 253, 254, 255 };

        private int[] isFour = new int[]{3, 6, 7, 12, 4, 15, 24, 28,
           30, 48, 56, 60, 96, 112, 120, 129, 131, 135, 192, 193, 195, 224, 225 };


        public MainWindow()
        {
            InitializeComponent();
        }

        private void loadFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Pliki zdjęć (*.png;*.jpeg)|*.png;*.jpeg;*.jpg;*.bmp";
            if (openFileDialog.ShowDialog() == true)
            {
                img.Source = new BitmapImage(new Uri(openFileDialog.FileName));
            }

            bmp = BitmapImage2Bitmap((BitmapImage)img.Source);
            pixels = new System.Drawing.Color[bmp.Width, bmp.Height];

            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    pixels[i, j] = bmp.GetPixel(i, j);
                }
            }
        }

        private void paintImage()
        {
            Bitmap bmp = BitmapImage2Bitmap((BitmapImage)img.Source);

            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    bmp.SetPixel(i, j, pixels[i, j]);
                }
            }

            img.Source = Bitmap2BitmapImage(bmp);
        }

        private System.Drawing.Color[,] performThresholding(int level)
        {
            System.Drawing.Color[,] result = new System.Drawing.Color[bmp.Width, bmp.Height];

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {

                    int b = pixels[i, j].R;

                    if (b > level)
                    {
                        result[i, j] = System.Drawing.Color.FromArgb(255, 255, 255);
                    }

                    else
                    {
                        result[i, j] = System.Drawing.Color.FromArgb(0, 0, 0);
                    }
                }
            }

            return result;

        }

        private void kmm(object sender, RoutedEventArgs e)
        {
            pixels = performThresholding(150);
            initializeMatrix();

            
            while (true)
            {
                int[,] matrixCopy = (int[,])pixelsMatrix.Clone();
                performKMM();
              

                if(!matrixChanged(matrixCopy))
                {
                    break;
                }
            }

            renderImage();
            paintImage();
        }

        private void performKMM()
        {
            prepareMatrix();
            deletePixels();
        }

        private void k3m(object sender, RoutedEventArgs e)
        {
            pixels = performThresholding(150);
            initializeMatrix();

            while(true)
            {
                int[,] matrixCopy = (int[,])pixelsMatrix.Clone();
                performPhases();

                
                if (!matrixChanged(matrixCopy))
                {
                    break;
                }

            }
            middlePhases(6);

            renderImage();
            paintImage();
        }


        private void performPhases()
        {
            phase0();
            middlePhases(1);
            middlePhases(2);
            middlePhases(3);
            middlePhases(4);
            middlePhases(5);
        }

        private void phase0()
        {
            int[] A0 = new int[] {3, 6, 7, 12, 14, 15, 24, 28, 30, 31, 48, 56, 60,
                        62, 63, 96, 112, 120, 124, 126, 127, 129, 131, 135,
                        143, 159, 191, 192, 193, 195, 199, 207, 223, 224,
                        225, 227, 231, 239, 240, 241, 243, 247, 248, 249,
                        251, 252, 253, 254};


            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    if (pixelsMatrix[i, j] != 0)
                    {
                        int weigth = calculateWeigth(i, j);
                        for (int k = 0; k < A0.Length; k++)
                        {
                            if (weigth == A0[k])
                            {
                                pixelsMatrix[i, j] = 2;
                            }
                        }
                    }
                }
            }
        }

        private void middlePhases(int n)
        {
            int[] A1 = new int[] { 7, 14, 28, 56, 112, 131, 193, 224 };
            int[] A2 = new int[] {7, 14, 15, 28, 30, 56, 60, 112, 120, 131, 135,
                                193, 195, 224, 225, 240};
            int[] A3 = new int[] {7, 14, 15, 28, 30, 31, 56, 60, 62, 112, 120,
                                124, 131, 135, 143, 193, 195, 199, 224, 225, 227,
                                240, 241, 248};
            int[] A4 = new int[] {7, 14, 15, 28, 30, 31, 56, 60, 62, 63, 112, 120,
                                124, 126, 131, 135, 143, 159, 193, 195, 199, 207,
                                224, 225, 227, 231, 240, 241, 243, 248, 249, 252};
            int[] A5 = new int[] {7, 14, 15, 28, 30, 31, 56, 60, 62, 63, 112, 120,
                                124, 126, 131, 135, 143, 159, 191, 193, 195, 199,
                                207, 224, 225, 227, 231, 239, 240, 241, 243, 248,
                                249, 251, 252, 254};
            int[] A6 = new int[] {3, 6, 7, 12, 14, 15, 24, 28, 30, 31, 48, 56,
                                60, 62, 63, 96, 112, 120, 124, 126, 127, 129, 131,
                                135, 143, 159, 191, 192, 193, 195, 199, 207, 223,
                                224, 225, 227, 231, 239, 240, 241, 243, 247, 248,
                                249, 251, 252, 253, 254};

            int[] checkMatrix;

            switch (n)
            {
                case 1:
                    checkMatrix = A1;
                    break;
                case 2:
                    checkMatrix = A2;
                    break;
                case 3:
                    checkMatrix = A3;
                    break;
                case 4:
                    checkMatrix = A4;
                    break;
                case 5:
                    checkMatrix = A5;
                    break;
                default:
                    checkMatrix = A6;
                    break;
            }

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    if(pixelsMatrix[i, j] == 2)
                    {
                        for(int k=0; k<checkMatrix.Length; k++)
                        {
                            if(calculateWeigth(i, j) == checkMatrix[k])
                            {
                                pixelsMatrix[i, j] = 0; 
                            }
                        }
                    }
                }
            }
        }

        private bool matrixChanged(int [,] matrixCopy)
        {
            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    if (matrixCopy[i, j] != pixelsMatrix[i, j])
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void renderImage()
        {
            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    if(pixelsMatrix[i,j] == 0)
                    {
                        pixels[i, j] = System.Drawing.Color.FromArgb(255, 255, 255);
                    }
                    else
                    {
                        pixels[i, j] = System.Drawing.Color.FromArgb(0,0,0);
                    }
                }
            }
        }

        private void initializeMatrix()
        {
            pixelsMatrix = new int[pixels.GetLength(0), pixels.GetLength(1)];

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    if (pixels[i, j] == System.Drawing.Color.FromArgb(255, 255, 255))
                    {
                        pixelsMatrix[i, j] = 0;
                    }
                    else
                    {
                        pixelsMatrix[i, j] = 1;
                    }
                }
            }
        }

        private void prepareMatrix()
        {

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    if (pixelsMatrix[i, j] != 0)
                    {
                        if (i != 0)
                        {
                            if (j != 0)
                            {
                                if (pixelsMatrix[i - 1, j - 1] == 0)
                                {
                                    pixelsMatrix[i, j] = 3;
                                }
                            }
                            if (j != pixels.GetLength(1) - 1)
                            {
                                if (pixelsMatrix[i - 1, j + 1] == 0)
                                {
                                    pixelsMatrix[i, j] = 3;
                                }
                            }
                        }
                        if (i != pixels.GetLength(0) - 1)
                        {
                            if (j != 0)
                            {
                                if (pixelsMatrix[i + 1, j - 1] == 0)
                                {
                                    pixelsMatrix[i, j] = 3;
                                }
                            }
                            if (j != pixels.GetLength(1) - 1)
                            {
                                if (pixelsMatrix[i + 1, j + 1] == 0)
                                {
                                    pixelsMatrix[i, j] = 3;
                                }
                            }
                        }
                        if (i != 0)
                        {
                            if (pixelsMatrix[i - 1, j] == 0)
                            {
                                pixelsMatrix[i, j] = 2;
                            }

                        }
                        if (i != pixels.GetLength(0) - 1)
                        {
                            if (pixelsMatrix[i + 1, j] == 0)
                            {
                                pixelsMatrix[i, j] = 2;
                            }
                        }
                        if (j != 0)
                        {
                            if (pixelsMatrix[i, j - 1] == 0)
                            {
                                pixelsMatrix[i, j] = 2;
                            }

                        }
                        if (j != pixels.GetLength(1) - 1)
                        {
                            if (pixelsMatrix[i, j + 1] == 0)
                            {
                                pixelsMatrix[i, j] = 2;
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    if (pixelsMatrix[i, j] != 0)
                    {

                        int sumForPixel = calculateWeigth(i, j);

                        for (int k = 0; k < isFour.Length; k++)
                        {
                            if (isFour[k] == sumForPixel)
                            {
                                pixelsMatrix[i, j] = 4;
                            }
                        }
                    }

                }
            }
        }

        private void deletePixels()
        {
            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    if(pixelsMatrix[i, j] == 4)
                    {
                        pixelsMatrix[i, j] = 0;
                    }
                }
            }

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    if(pixelsMatrix[i, j] == 2)
                    {
                        processPixel(i, j);
                    }
                }
            }

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    if (pixelsMatrix[i, j] == 3)
                    {
                        processPixel(i, j);
                    }
                }
            }
        }

        private void processPixel(int i, int j)
        {
            int weigth = calculateWeigth(i, j);
            for(int k = 0; k<codesToDelete.Length; k++)
            {
                if(weigth == codesToDelete[k])
                {
                    pixelsMatrix[i, j] = 0;
                }
            }
            if(pixelsMatrix[i,j] != 0)
            {
                pixelsMatrix[i, j] = 1;
            }
        }

        private int calculateWeigth(int i, int j)
        {
            int sumForPixel = 0;

            if (i != 0)
            {
                if (j != 0)
                {
                    if (pixelsMatrix[i - 1, j - 1] != 0)
                    {
                        sumForPixel += 32;
                    }
                }
                if (j != pixels.GetLength(1) - 1)
                {
                    if (pixelsMatrix[i - 1, j + 1] != 0)
                    {
                        sumForPixel += 128;
                    }
                }
            }

            if (i != pixels.GetLength(0) - 1)
            {
                if (j != 0)
                {
                    if (pixelsMatrix[i + 1, j - 1] != 0)
                    {
                        sumForPixel += 8;
                    }
                }
                if (j != pixels.GetLength(1) - 1)
                {
                    if (pixelsMatrix[i + 1, j + 1] != 0)
                    {
                        sumForPixel += 2;
                    }
                }
            }
            if (i != 0)
            {
                if (pixelsMatrix[i - 1, j] != 0)
                {
                    sumForPixel += 64;
                }

            }
            if (i != pixels.GetLength(0) - 1)
            {
                if (pixelsMatrix[i + 1, j] != 0)
                {
                    sumForPixel += 4;
                }
            }
            if (j != 0)
            {
                if (pixelsMatrix[i, j - 1] != 0)
                {
                    sumForPixel += 16;
                }

            }
            if (j != pixels.GetLength(1) - 1)
            {
                if (pixelsMatrix[i, j + 1] != 0)
                {
                    sumForPixel += 1;
                }
            }

            return sumForPixel;
        }

        private Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {

            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);
                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(outStream);

                return new Bitmap(bitmap);
            }
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();

                return bitmapImage;
            }
        }

    }
}
